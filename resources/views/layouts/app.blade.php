<?php 
$url = explode("/",url()->current());
if (empty($url[3])) {
    $url[3] = 'home';
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Bredh flat responsive HTML & WHMCS hosting and domains template">
    <meta name="author" content="coodiv.net (nedjai mohamed)">
    <link rel="icon" href="favicon.ico">
    <title> @yield('title') - PUSAT KARIR STMIK SUMEDANG  </title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/front/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- main css file -->
    <link href="{{asset('assets/front/css/main.min.css')}}" rel="stylesheet">
    @yield('css')

</head>

<body><!-- start body -->

    <div class="preloader"><!-- start preloader -->
        <div class="preloader-container">
            <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <circle fill="#675cda" stroke="none" cx="6" cy="50" r="6">
                    <animateTransform attributeName="transform" dur="1s" type="translate" values="0 15 ; 0 -15; 0 15" repeatCount="indefinite" begin="0.1" />
                </circle>
                <circle fill="#675cda" stroke="none" cx="30" cy="50" r="6">
                    <animateTransform attributeName="transform" dur="1s" type="translate" values="0 10 ; 0 -10; 0 10" repeatCount="indefinite" begin="0.2" />
                </circle>
                <circle fill="#675cda" stroke="none" cx="54" cy="50" r="6">
                    <animateTransform attributeName="transform" dur="1s" type="translate" values="0 5 ; 0 -5; 0 5" repeatCount="indefinite" begin="0.3" />
                </circle>
            </svg>
            <span>loading</span>
        </div>
    </div><!-- end preloader -->

    <div id="coodiv-header" class="d-flex mx-auto flex-column {{($url[3] != 'home') ? 'subpages-header' : null}} moon-edition"><!-- start header -->
        <div class="bg_overlay_header">
        <div class="video-bg-nuhost-header" @if(View::hasSection('bg-image'))
                                                @yield('bg-image')
                                            @else
                                                style="background: url('assets/front/media/wisuda.jpeg') no-repeat;width:100%;background-size:cover;"
                                            @endif>
		<div id="video_cover"></div>
		<video autoplay muted loop>
		<source src="media/coodiv-vid.mp4" type="video/mp4">
		</video>
		<span class="video-bg-nuhost-header-bg"></span>
		</div>
        </div>
        <!-- Fixed navbar -->
        <nav id="coodiv-navbar-header" class="navbar navbar-expand-md fixed-header-layout">
            <div class="container main-header-coodiv-s">
                <a class="navbar-brand" href="index.html">
				<img class="w-logo" src="img/header/logo-w.png" alt="" />
				<img class="b-logo" src="img/header/logo.png" alt="" />
				</a>
                <button class="navbar-toggle offcanvas-toggle menu-btn-span-bar ml-auto" data-toggle="offcanvas" data-target="#offcanvas-menu-home">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse navbar-offcanvas" id="offcanvas-menu-home">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{($url[3] == 'home') ? 'active' : null}}">
                            <a class="nav-link" href="{{url('/')}}">Home</a>
                        </li>
                        <li class="nav-item {{($url[3] == 'bimbingan-karir') ? 'active' : null}}">
                            <a class="nav-link" href="{{url('bimbingan-karir')}}">Bimbingan Karir</a>
                        </li>
                        <li class="nav-item {{($url[3] == 'lowongan-kerja') ? 'active' : null}}">
                            <a class="nav-link" href="{{url('lowongan-kerja')}}">Lowongan Kerja</a>
                        </li>
                        <li class="nav-item {{($url[3] == 'magang') ? 'active' : null}}">
                            <a class="nav-link" href="{{url('magang')}}">Magang</a>
                        </li>
                        <li class="nav-item {{($url[3] == 'acara') ? 'active' : null}}">
                            <a class="nav-link" href="{{url('acara')}}">Acara</a>
                        </li>
                        <li class="nav-item {{($url[3] == 'pelatihan') ? 'active' : null}}">
                            <a class="nav-link" href="{{url('pelatihan')}}">Pelatihan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="mt-auto header-top-height"></div>
        @if ($url[3] == 'home')
        <main class="container mb-auto">
            <div class="carousel carousel-main">
                <div class="carousel-cell">
                    <h3 class="main-header-text-title"><span>Hallo, Selamat Datang di Official Website</span>Pusat Karir STMIK Sumedang</h3>
					<div class="row justify-content-center domain-search-row">
                    {{-- <form action="domains.html" id="domain-search-header" class="col-md-7">
                        <i class="fas fa-globe"></i>
                        <input type="text" placeholder="Masukan kata kunci" id="domain" name="domains">
                        <span class="inline-button-domain-order">
                  	  <button data-toggle="tooltip" data-placement="left" title="transfer" id="transfer-btn" type="submit" name="transfer" value="Transfer"><i class="fas fa-undo"></i></button>
                  	  <button data-toggle="tooltip" data-placement="left" title="search" id="search-btn" type="submit" name="submit" value="Search"><i class="fas fa-search"></i></button>
						</div>
                    </form> --}}
                    <section class="mt-5">
                        <div class="container">
                            <div class="row justify-content-center futures-version-2 text-center">
                                <div class="col-md-4">
                                    <a href="{{url('bimbingan-karir')}}" style="all:unset;cursor:pointer;">
                                        <div class="futures-version-2-box">
                                                <i class="bredhicon-target"></i>
                                                <h5>Bimbingan Karir</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{url('lowongan-kerja')}}" style="all:unset;cursor:pointer;">
                                        <div class="futures-version-2-box">
                                                <i class="bredhicon-doc-alt"></i>
                                                <h5>Lowongan Kerja</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{url('magang')}}" style="all:unset;cursor:pointer;">
                                        <div class="futures-version-2-box">
                                                <i class="bredhicon-link"></i>
                                                <h5>Magang</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4 mt-5">
                                    <a href="{{url('acara')}}" style="all:unset;cursor:pointer;">
                                        <div class="futures-version-2-box">
                                                <i class="bredhicon-award"></i>
                                                <h5>Acara</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4 mt-5">
                                    <a href="{{url('pelatihan')}}" style="all:unset;cursor:pointer;">
                                        <div class="futures-version-2-box">
                                                <i class="bredhicon-split"></i>
                                                <h5>Pelatihan</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>	
                    </section>
					</div>
                </div>
            </div>
        </main>
        @else
        <main class="container mb-auto">
            <h3 class="mt-3 main-header-text-title"><span>Pusat Karir STMIK Sumedang</span>@yield('title')
            <p class="sub-page-breadcrumb"><a href="{{url('/')}}">homepage</a> - @yield('title') </p>
            </h3>
        </main>
        @endif
        
        <div class="mt-auto"></div>
    </div><!-- end header -->

	@yield('content')

    <footer class="footer-section" style="padding:30px;
            left: 0;
            bottom: 0;
            width: 100%;
            text-align: center;" >
        <div class="container">
            <div class="row justify-content-between final-footer-area">
                <div class="final-footer-area-text">
                    © Copyright 2021 by TAHUNGODING 
                </div>
            </div>
        </div>
    </footer>

    <!-- jquery -->
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <!-- bootstrap JavaScript -->
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- template JavaScript -->
    <script src="{{asset('assets/front/js/template-scripts.js')}}"></script>
    <!-- flickity JavaScript -->
    <script src="{{asset('assets/front/js/flickity.pkgd.min.js')}}"></script>
    <!-- carousel JavaScript -->
    <script src="{{asset('assets/front/owlcarousel/owl.carousel.min.js')}}"></script>
    <!-- parallax JavaScript -->
    <script src="{{asset('assets/front/js/parallax.min.j')}}s"></script>
    <!-- mailchamp JavaScript -->
    <script src="{{asset('assets/front/js/mailchamp.js')}}"></script>
    <!-- bootstrap offcanvas -->
    <script src="{{asset('assets/front/js/bootstrap.offcanvas.min.j')}}s"></script>
    <!-- touchSwipe JavaScript -->
    <script src="{{asset('assets/front/js/jquery.touchSwipe.min.js')}}"></script>
	<!-- seconde style additionel JavaScript -->
	<script src="{{asset('assets/front/js/particles-code.js')}}"></script>
	<script src="{{asset('assets/front/js/particles.js')}}"></script>
	<script src="{{asset('assets/front/js/smoothscroll.js')}}"></script>
	<!-- video background JavaScript -->
    <script src="{{asset('assets/front/js/video-bg.js')}}"></script>

    @yield('js')

	</body>

</html>

