<?php 
$url = explode("/",url()->current());
$profilePic = url('assets/back/images/avatars/thumb-3.jpg');
if (!empty(Auth::user()->picture)) {
    $profilePic = Storage::url(Auth::user()->picture);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/back/images/logo/favicon-pk.png')}}">

    <!-- page css -->
    @yield('css')

    <!-- Core css -->
    <link href="{{asset('assets/back/css/app.min.css')}}" rel="stylesheet">

</head>

<body>
    <div class="app">
        <div class="layout">
            <!-- Header START -->
            <div class="header">
                <div class="logo logo-dark">
                    <a href="{{'dashboard'}}">
                        <img src="{{asset('assets/back/images/logo/logo-pk.png')}}" alt="Logo">
                        <img class="logo-fold" src="{{asset('assets/back/images/logo/logo-fold-pk.png')}}" alt="Logo">
                    </a>
                </div>
                <div class="logo logo-white">
                    <a href="{{'dashboard'}}">
                        <img src="{{asset('assets/back/images/logo/logo-white-pk.png')}}" alt="Logo">
                        <img class="logo-fold" src="{{asset('assets/back/images/logo/logo-fold-white-pk.png')}}" alt="Logo">
                    </a>
                </div>
                <div class="nav-wrap">
                    <ul class="nav-left">
                        <li class="desktop-toggle">
                            <a href="javascript:void(0);">
                                <i class="anticon"></i>
                            </a>
                        </li>
                        <li class="mobile-toggle">
                            <a href="javascript:void(0);">
                                <i class="anticon"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="dropdown dropdown-animated scale-left">
                            <a href="javascript:void(0);" data-toggle="dropdown">
                                <i class="anticon anticon-bell notification-badge"></i>
                            </a>
                            <div class="dropdown-menu pop-notification">
                                <div class="p-v-15 p-h-25 border-bottom d-flex justify-content-between align-items-center">
                                    <p class="text-dark font-weight-semibold m-b-0">
                                        <i class="anticon anticon-bell"></i>
                                        <span class="m-l-10">Notification</span>
                                    </p>
                                    <a class="btn-sm btn-default btn" href="javascript:void(0);">
                                        <small>View All</small>
                                    </a>
                                </div>
                                <div class="relative">
                                    <div class="overflow-y-auto relative scrollable" style="max-height: 300px">
                                        <a href="javascript:void(0);" class="dropdown-item d-block p-15 border-bottom">
                                            <div class="d-flex">
                                                <div class="avatar avatar-blue avatar-icon">
                                                    <i class="anticon anticon-mail"></i>
                                                </div>
                                                <div class="m-l-15">
                                                    <p class="m-b-0 text-dark">Fitur Notifikasi</p>
                                                    <p class="m-b-0"><small>Cooming Soon..</small></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown dropdown-animated scale-left">
                            <div class="pointer" data-toggle="dropdown">
                                <div class="avatar avatar-image  m-h-10 m-r-15">
                                    <img src="{{$profilePic}}"  alt="">
                                </div>
                            </div>
                            <div class="p-b-15 p-t-20 dropdown-menu pop-profile">
                                <div class="p-h-20 p-b-15 m-b-10 border-bottom">
                                    <div class="d-flex m-r-50">
                                        <div class="">
                                            <img src="{{$profilePic}}" alt="" style="width:50px;height:50px;border-radius:30px;object-fit:cover;">
                                        </div>
                                        <div class="m-l-10">
                                            <p class="m-b-0 text-dark font-weight-semibold">{{Auth::user()->fullname}}</p>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{route('users.edit', Auth::user()->id)}}" class="dropdown-item d-block p-h-15 p-v-10">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div>
                                            <i class="anticon opacity-04 font-size-16 anticon-user"></i>
                                            <span class="m-l-10">Ubah Profile</span>
                                        </div>
                                    </div>
                                </a>
                                <a href="{{ url('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();" class="dropdown-item d-block p-h-15 p-v-10">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div>
                                            <i class="anticon opacity-04 font-size-16 anticon-logout"></i>
                                            <span class="m-l-10">Logout</span>
                                        </div>
                                    </div>
                                </a>
                                <form id="logout-form" action="{{ url('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#quick-view">
                                <i class="anticon anticon-appstore"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>    
            <!-- Header END -->

            <!-- Side Nav START -->
            <div class="side-nav">
                <div class="side-nav-inner">
                    <ul class="side-nav-menu scrollable">
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle active" href="{{url('dashboard')}}">
                                <span class="icon-holder">
                                    <i class="anticon anticon-dashboard"></i>
                                </span>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        @if (Auth::user()->user_level_id == 1)
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="anticon anticon-user"></i>
                                </span>
                                <span class="title">Pengguna</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{($url[3] == 'users') ? 'active' : null}}">
                                    <a href="{{url('users')}}">List</a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
									<i class="anticon anticon-build"></i>
								</span>
                                <span class="title">Bimbingan Karir</span>
                                <span class="arrow">
									<i class="arrow-icon"></i>
								</span>
                            </a>
                            <ul class="dropdown-menu">
                                @if (Auth::user()->user_level_id == 1)
                                <li class="{{($url[3] == 'career_guidances') ? 'active' : null}}">
                                    <a href="{{url('career_guidances')}}">List</a>
                                </li>
                                @endif
                                <li class="{{($url[3] == 'career_guidance_alumnis') ? 'active' : null}}">
                                    <a href="{{url('career_guidance_alumnis')}}">List Alumni</a>
                                </li>
                                @if (Auth::user()->user_level_id == 1)
                                <li class="{{($url[3] == 'career_guidance_users') ? 'active' : null}}">
                                    <a href="{{url('career_guidance_users')}}">Pembimbing</a>
                                </li>
                                <li class="{{($url[3] == 'career_guidance_schedules') ? 'active' : null}}">
                                    <a href="{{url('career_guidance_schedules')}}">Jadwal</a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        @if (Auth::user()->user_level_id == 1)
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="anticon anticon-hdd"></i>
                                </span>
                                <span class="title">Lowongan Kerja</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{($url[3] == 'job_vacancies') ? 'active' : null}}">
                                    <a href="{{url('job_vacancies')}}">List</a>
                                </li>
                                <li class="{{($url[3] == 'job_vacancy_categories') ? 'active' : null}}">
                                    <a href="{{url('job_vacancy_categories')}}">Kategori</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="anticon anticon-laptop"></i>
                                </span>
                                <span class="title">Magang</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{($url[3] == 'internships') ? 'active' : null}}">
                                    <a href="{{url('internships')}}">List</a>
                                </li>
                                <li class="{{($url[3] == 'internship_categories') ? 'active' : null}}">
                                    <a href="{{url('internship_categories')}}">Kategori</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="anticon anticon-form"></i>
                                </span>
                                <span class="title">Acara</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{($url[3] == 'events') ? 'active' : null}}">
                                    <a href="{{url('events')}}">List</a>
                                </li>
                                <li class="{{($url[3] == 'event_categories') ? 'active' : null}}">
                                    <a href="{{url('event_categories')}}">Kategori</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="anticon anticon-solution"></i>
                                </span>
                                <span class="title">Pelatihan</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{($url[3] == 'trainings') ? 'active' : null}}">
                                    <a href="{{url('trainings')}}">List</a>
                                </li>
                                <li class="{{($url[3] == 'training_categories') ? 'active' : null}}">
                                    <a href="{{url('training_categories')}}">Kategori</a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle active" href="{{ url('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                                <span class="icon-holder">
                                    <i class="anticon anticon-logout"></i>
                                </span>
                                <span class="title">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ url('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Side Nav END -->

            <!-- Page Container START -->
            <div class="page-container">
                
                <!-- Content Wrapper START -->
                @yield('content')
                <!-- Content Wrapper END -->

                <!-- Footer START -->
                <footer class="footer">
                    <div class="footer-content">
                        <p class="m-b-0">Copyright © 2021 TAHUNGODING.</p>
                        {{-- <span>
                            <a href="" class="text-gray m-r-15">Term &amp; Conditions</a>
                            <a href="" class="text-gray">Privacy &amp; Policy</a>
                        </span> --}}
                    </div>
                </footer>
                <!-- Footer END -->

            </div>
            <!-- Page Container END -->

            <!-- Quick View START -->
            <div class="modal modal-right fade quick-view" id="quick-view">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header justify-content-between align-items-center">
                            <h5 class="modal-title">Theme Config</h5>
                        </div>
                        <div class="modal-body scrollable">
                            <div class="m-b-30">
                                <h5 class="m-b-0">Header Color</h5>
                                <p>Config header background color</p>
                                <div class="theme-configurator d-flex m-t-10">
                                    <div class="radio">
                                        <input id="header-default" name="header-theme" type="radio" checked value="default">
                                        <label for="header-default"></label>
                                    </div>
                                    <div class="radio">
                                        <input id="header-primary" name="header-theme" type="radio" value="primary">
                                        <label for="header-primary"></label>
                                    </div>
                                    <div class="radio">
                                        <input id="header-success" name="header-theme" type="radio" value="success">
                                        <label for="header-success"></label>
                                    </div>
                                    <div class="radio">
                                        <input id="header-secondary" name="header-theme" type="radio" value="secondary">
                                        <label for="header-secondary"></label>
                                    </div>
                                    <div class="radio">
                                        <input id="header-danger" name="header-theme" type="radio" value="danger">
                                        <label for="header-danger"></label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div>
                                <h5 class="m-b-0">Side Nav Dark</h5>
                                <p>Change Side Nav to dark</p>
                                <div class="switch d-inline">
                                    <input type="checkbox" name="side-nav-theme-toogle" id="side-nav-theme-toogle">
                                    <label for="side-nav-theme-toogle"></label>
                                </div>
                            </div>
                            <hr>
                            <div>
                                <h5 class="m-b-0">Folded Menu</h5>
                                <p>Toggle Folded Menu</p>
                                <div class="switch d-inline">
                                    <input type="checkbox" name="side-nav-fold-toogle" id="side-nav-fold-toogle">
                                    <label for="side-nav-fold-toogle"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
            <!-- Quick View END -->
        </div>
    </div>

    
    <!-- Core Vendors JS -->
    <script src="{{asset('assets/back/js/vendors.min.js')}}"></script>

    <!-- page js -->
    @yield('js')

    <!-- Core JS -->
    <script src="{{asset('assets/back/js/app.min.js')}}"></script>

</body>

</html>