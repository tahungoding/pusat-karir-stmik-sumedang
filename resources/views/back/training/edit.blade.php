@extends('back')
@section('title')
    Ubah Pelatihan
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/quill/quill.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>
<script src="{{ asset('ckeditor.js')}}"></script>
<script>CKEDITOR.replace( 'article-ckeditor', {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
});</script>
<script>
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#thumbnail-preview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#thumbnail").change(function(){
        readURL(this);
    });
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('trainings')}}">Pelatihan</a>
                <span class="breadcrumb-item active">Ubah</span>
            </nav>
        </div>
    </div>
    <div class="card col-md-8">
        <div class="card-body ">
            <h4>Ubah Pelatihan</h4>
            <div class="m-t-25">
                <form action="{{route('trainings.update', $detail->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Judul<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('judul') is-invalid @enderror" name="judul" value="{{$detail->judul??old('judul')}}" placeholder="Judul">
                        @error('judul')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Thumbnail<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <br>
                        <img src="{{Storage::url($detail->thumbnail)}}" id="thumbnail-preview" class="mt-1 mb-2" alt="" width="350px" height="240px" style="object-fit:cover;">
                        <br>
                        <small>Image 350px*240px</small>
                        <input type="file" class="form-control m-b-15 @error('thumbnail') is-invalid @enderror" id="thumbnail" name="thumbnail"  placeholder="Thumbnail">
                        @error('thumbnail')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Isi<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <textarea name="deskripsi" class="form-control m-b-15 @error('deskripsi') is-invalid @enderror" id="article-ckeditor" cols="30" rows="10">{{$detail->deskripsi??old('deskripsi')}}</textarea>
                        <br>
                        @error('deskripsi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Kategori<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <select name="category_id" class="form-control m-b-15 @error('category_id') is-invalid @enderror" id="">
                            <option value="">Pilih Kategori</option>
                            @foreach ($training_categories as $item)
                                <option value="{{$item->id}}" {{($item->id == $detail->category_id) ? 'selected' : null}}>{{$item->nama}}</option>
                            @endforeach
                        </select>
                        @error('deskripsi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary btn-md" title="Perbaharui Pelatihan"><i class="fas fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection