@extends('back')
@section('title')
    Tambah Pengguna
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/quill/quill.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>
<script type="text/javascript">
    function readURL(input) {
        $('#profile-img-tag').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('users')}}">Pengguna</a>
                <span class="breadcrumb-item active">Tambah</span>
            </nav>
        </div>
    </div>
    <div class="card col-md-4">
        <div class="card-body ">
            <h4>Tambah Pengguna</h4>
            <div class="m-t-25">
                <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nama Lengkap <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('fullname') is-invalid @enderror" name="fullname" value="{{old('fullname')}}" placeholder="Nama Lengkap">
                        @error('fullname')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Username <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('username') is-invalid @enderror" name="username" value="{{old('username')}}" placeholder="Username">
                        @error('username')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Password <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="password" class="form-control m-b-15 @error('password') is-invalid @enderror" name="password" value="{{old('password')}}" placeholder="Password">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Konfirmasi Password <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="password" class="form-control m-b-15 @error('confirm_password') is-invalid @enderror" name="confirm_password" value="{{old('confirm_password')}}" placeholder="Konfirmasi Password">
                        @error('confirm_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Email <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="email" class="form-control m-b-15 @error('email') is-invalid @enderror" name="email" value="{{old('email')}}" placeholder="Email">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Jenis Kelamin <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <div class="radio">
                            <input id="radio1" value="Laki-laki" name="gender" type="radio" checked="">
                            <label for="radio1">Laki-laki</label>
                        </div>
                        <div class="radio">
                            <input id="radio2" value="Perempuan" name="gender" type="radio">
                            <label for="radio2">Perempuan</label>
                        </div>
                        @error('gender')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Tanggal Lahir</label>
                        <input type="date" class="form-control m-b-15 @error('birthday') is-invalid @enderror" name="birthday" value="{{old('birhtday')}}" placeholder="Tanggal Lahir">
                        @error('birthday')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nomor Telephone</label>
                        <input type="number" class="form-control m-b-15 @error('phone') is-invalid @enderror" name="phone" value="{{old('phone')}}" placeholder="Nomor Telepon">
                        @error('phone')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Foto</label>
                        <input type="file" class="form-control m-b-15 @error('picture') is-invalid @enderror" name="picture" id="profile-img">
                        @error('file')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <img src="" id="profile-img-tag" style="width:100px;height:100px;object-fit:cover;display:none" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary btn-md" title="Tambah Pengguna"><i class="anticon anticon-user-add"></i> Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection