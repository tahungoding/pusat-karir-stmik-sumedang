@extends('back')
@section('title')
    Detail - List Bimbingan Karir Alumni
@endsection
@section('js')
<script src="assets/js/pages/icon.js"></script>
@if (Session::get('success') || Session::get('error'))
<script>
    $('#tutup').modal('show');
</script>
@endif
@error('catatan')
<script>
    $('#tutup').modal('show');
</script>
@enderror
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('career_guidance_alumnis')}}">Bimbingan Karir Alumni</a>
                <span class="breadcrumb-item active">Detail</span>
            </nav>
        </div>
    </div>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h2>Data Personal</h2>
                <h5>Nama Lengkap</h5>
                <p>{{$detail->nama_lengkap}}</p>
                <hr>
                <h5>Program Studi</h5>
                <p>{{$detail->program_studi}}</p>
                <hr>
                <h5>Pembimbing</h5>
                <p>{{$detail->pembimbing->nama_lengkap}}</p>
                <hr>
                <h5>Jenis Kelamin</h5>
                <p>{{$detail->jenis_kelamin}}</p>
                <hr>
                <h5>Tempat Lahir</h5>
                <p>{{$detail->tempat_lahir}}</p>
                <hr>
                <h5>Tanggal Lahir</h5>
                <p>{{$detail->tanggal_lahir}}</p>
                <hr>
                <h5>Email</h5>
                <p>{{$detail->email}}</p>
                <hr>
                <h5>Telepon</h5>
                <p>{{$detail->telepon}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h2>Data Bimbingan 
                    @if ($detail->status == 'Open')
                    <i class="fas fa-door-open text-success" title="Open"></i>
                    @elseif ($detail->status == 'Close')
                    <i class="fas fa-door-closed text-danger" title="Close"></i>
                    @endif    
                </h2>
                <h5>Area Permasalahan</h5>
                <p>{{$detail->area_permasalahan}}</p>
                <hr>
                <h5>Kesediaan mengikuti sesi konsultasi secara individual maupun kelompok</h5>
                <p>
                    <?php if ($detail->sesi == "individual") {
                        echo 'Bersedia mengikuti sesi konsultasi individual saja';
                    } elseif ($detail->sesi == "kelompok") {
                        echo 'Bersedia mengikuti sesi konsultasi Kelompok saja';
                    } elseif ($detail->sesi == "keduanya") {
                        echo 'Bersedia mengikuti sesi konsultasi Individual maupun kelompok';
                    }
                     ?>    
                </p>
                <hr>
                <h5>Alasan pilihan sesi</h5>
                <p>{{$detail->alasan_sesi}}</p>
                <hr>
                <h5>Keterangan Diri Klien</h5>
                <ul>
                    @foreach ($keterangan_diri as $item)
                        <li>{{$item->keterangan}}</li>
                    @endforeach
                </ul>
                <hr>
                <h5>Jadwal Pilihan Bimbingan</h5>
                <ul>
                    @foreach ($jadwal as $item)
                        <li>{{$item->pembimbing->nama_lengkap}} | {{ucfirst($item->hari)}}, Pk {{date('H:i', strtotime($item->waktu_awal))}} - {{date('H:i', strtotime($item->waktu_selesai))}} WIB</li>
                    @endforeach
                </ul>
                <hr>
                <h5>CV</h5>
                <?php
                $get_ext = explode(".", $detail->cv);
                $get_path = explode("/", $detail->cv);
                ?> 
                {{-- @if ($get_ext[1] != 'pdf')
                    <iframe src='https://view.officeapps.live.com/op/embed.aspx?src={{Storage::url($detail->cv)}}'>
                    </iframe>
                @else 
                    <iframe src="https://docs.google.com/viewer?url={{url(Storage::url($detail->cv))}}&embedded=true"></iframe>
                @endif  --}}
                <a href="{{Storage::url($detail->cv)}}" download>{{last($get_path)}}</a>
                @if ($detail->status == 'Open' && Auth::user()->user_level_id == 1)
                    <hr>
                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#tutup">
                        <i class="fas fa-door-closed"></i> Tutup Bimbingan ?
                    </button>
                @else
                    <hr>
                    <h5>Catatan</h5>
                    {{($detail->catatan != null) ? $detail->catatan : 'Tidak ada catatan.'}}
                @endif
            </div>
        </div>
    </div>
    <div class="modal fade" id="tutup">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tutupTitle">Tutup Bimbingan</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="anticon anticon-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                        @if ($msg = Session::get('success'))
                                    <div class="alert alert-success">
                                        {{$msg}}
                                    </div>
                            @endif
                            @if ($msg = Session::get('error'))
                                    <div class="alert alert-danger">
                                        {{$msg}}
                                    </div>
                            @endif
                    <form action="{{route('close_career_guidance_alumni', $detail->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-md-12">
                        <label for="">Berikan Catatan <sup>(Optional)</sup></label>
                        <textarea name="catatan" class="form-control m-b-15 @error('catatan') is-invalid @enderror" id="" cols="30" rows="10"></textarea>
                        @error('catatan')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fas fa-door-closed"></i> Tutup Bimbingan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection