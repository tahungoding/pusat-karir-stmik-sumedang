@extends('back')
@section('title')
    Tambah Pembimbing
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/quill/quill.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('career_guidance_users')}}">Pembimbing</a>
                <span class="breadcrumb-item active">Tambah</span>
            </nav>
        </div>
    </div>
    <div class="card col-md-4">
        <div class="card-body ">
            <h4>Tambah Pembimbing</h4>
            <div class="m-t-25">
                <form action="{{route('career_guidance_users.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label for="">NIDN<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('nidn') is-invalid @enderror" name="nidn" value="{{old('nidn')}}" placeholder="NIDN">
                        @error('nidn')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nama Lengkap <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{old('nama_lengkap')}}" placeholder="Nama Lengkap">
                        @error('nama_lengkap')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Bidang Ilmu <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('bidang_ilmu') is-invalid @enderror" name="bidang_ilmu" value="{{old('bidang_ilmu')}}" placeholder="Bidang Ilmu">
                        @error('bidang_ilmu')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary btn-md" title="Tambah Pembimbing"><i class="fas fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection