@extends('layouts.app')
@section('title')
    Lowongan Kerja
@endsection
@section('bg-image')
 style="background: url('assets/front/media/loker.jpeg') no-repeat;width:100%;background-size:cover;object-fit:cover"
@endsection
@section('css')

<link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<section class="white-bg">
	<div class="container">
        <div class="row justify-content-center blog-items-home"><!-- start row -->
                <table class="table-responsive data-table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Tanggal Rilis</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>	
        </div>
	</section>
    <br>
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(function () {
      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          pageLength: 100,
          bInfo: false,
          responsive: true,
          ajax: "{{ route('lowongan-kerja.index') }}",
          columns: [
              {data: 'judul', name: 'judul'},
              {data: 'created_at', name: 'created_at'},
              {
                data: 'status', 
                name: 'status',
                render: function (data) {
                    if (data !== null) {
                        if (data === "Publish") {
                            return "<span class='badge badge-success'>Tersedia</span>";
                        } else {
                            return "<span class='badge badge-danger'>Tutup</span>";
                        }
                    } else {
                    return "Belum terdaftar";
                    }
                }
            },
          ]
      });
    });
  </script>
@endsection