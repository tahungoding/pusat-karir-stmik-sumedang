<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerGuidanceUser extends Model
{
    protected $fillable = [
        'nama_lengkap', 'bidang_ilmu', 'nidn'
    ];

    public function jadwal(){
        return $this->hasOne(CareerGuidanceSchedule::class);
    }
}
