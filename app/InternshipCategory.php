<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternshipCategory extends Model
{
    protected $fillable = [
        'nama', 'catatan'
    ];

}
