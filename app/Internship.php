<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Internship extends Model
{
    protected $fillable = [
        'author_id', 'category_id', 'judul', 'thumbnail', 'deskripsi', 'slug'
    ];

    public function category(){
        return $this->belongsTo(InternshipCategory::class, 'category_id');
    }

    public function author(){
        return $this->belongsTo(User::class, 'author_id');
    }
}

