<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareerGuidanceUser;
use Illuminate\Support\Facades\Storage;

class CareerGuidanceUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['career_guidance_users'] = CareerGuidanceUser::all();
        return view('back.career_guidance_users.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.career_guidance_users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nidn' => 'required|max:255',
            'nama_lengkap' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'bidang_ilmu' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
        ]);

        $pembimbing = new CareerGuidanceUser([
            'nidn' => $request->get('nidn'),
            'nama_lengkap' => $request->get('nama_lengkap'),
            'bidang_ilmu' => $request->get('bidang_ilmu'),
        ]);

        $pembimbing->save();
        return redirect('/career_guidance_users')->with('success', 'Pembimbing berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['career_guidance_user'] = CareerGuidanceUser::findOrFail($id);
        return view('back.career_guidance_users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nidn' => 'required|max:255',
            'nama_lengkap' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'bidang_ilmu' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
        ]);
        
        $pembimbing = CareerGuidanceUser::findOrFail($id);

        $pembimbing->nidn =  $request->get('nidn');
        $pembimbing->nama_lengkap =  $request->get('nama_lengkap');
        $pembimbing->bidang_ilmu = $request->get('bidang_ilmu');
        $pembimbing->save();

        return redirect('/career_guidance_users')->with('success', 'Pembimbing berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pembimbing = CareerGuidanceUser::findOrFail($id);
        if ($pembimbing->delete()) {
            return redirect('/career_guidance_users')->with('success', 'Pembimbing berhasil dihapus!');
        }else{
            return redirect('/career_guidance_users')->with('error', 'Pembimbing gagal dihapus!');
        }
    }
}
