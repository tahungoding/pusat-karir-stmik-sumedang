<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Internship;
use App\InternshipCategory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class InternshipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['internships'] = Internship::all();
        return view('back.internships.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['internship_categories'] = InternshipCategory::all();
        return view('back.internships.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'thumbnail' => 'required|mimes:jpg,jpeg,png,gif',
            'deskripsi' => 'required',
            'category_id' => 'required'
        ]);

        $path = ($request->thumbnail)
        ? $request->file('thumbnail')->store("/public/input/lowongan_kerja")
        : null;

        $lowongan_kerja = new Internship([
            'author_id' => Auth::user()->id,
            'judul' => $request->get('judul'),
            'slug' => Str::slug($request->get('judul')),
            'thumbnail' => $path,
            'deskripsi' => $request->get('deskripsi'),
            'category_id' => $request->get('category_id'),
        ]);

        $lowongan_kerja->save();
        return redirect('/internships')->with('success', 'Magang berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['detail'] = Internship::findOrFail($id);
        $data['internship_categories'] = InternshipCategory::all();
        return view('back.internships.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'thumbnail' => 'mimes:jpg,jpeg,png,gif',
            'deskripsi' => 'required',
            'category_id' => 'required'
        ]);
        
        $lowongan_kerja = Internship::findOrFail($id);
        $path = ($request->thumbnail)
        ? $request->file('thumbnail')->store("/public/input/lowongan_kerja")
        : null;


        if ($request->thumbnail) {
            Storage::delete($lowongan_kerja->thumbnail);
        }

        $lowongan_kerja->judul =  $request->get('judul');
        $lowongan_kerja->slug = Str::slug($request->get('slug'));
        $lowongan_kerja->deskripsi = $request->get('deskripsi');
        $lowongan_kerja->category_id = $request->get('category_id');
        $lowongan_kerja->thumbnail = $path;
        $lowongan_kerja->save();

        return redirect('/internships')->with('success', 'Magang berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lowongan_kerja = Internship::findOrFail($id);
        if ($lowongan_kerja->thumbnail) {
            Storage::delete($lowongan_kerja->thumbnail);
        }
        if ($lowongan_kerja->delete()) {
            return redirect('/internships')->with('success', 'Magang berhasil dihapus!');
        }else{
            return redirect('/internships')->with('error', 'Magang gagal dihapus!');
        }
    }
}
