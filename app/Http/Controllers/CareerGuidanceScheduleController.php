<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareerGuidanceSchedule;
use App\CareerGuidanceUser;

class CareerGuidanceScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['career_guidance_schedules'] = CareerGuidanceSchedule::all();
        return view('back.career_guidance_schedules.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['career_guidance_users'] = CareerGuidanceUser::all();
        return view('back.career_guidance_schedules.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'career_guidance_user_id' => 'required',
            'hari' => 'required',
            'waktu_awal' => 'required',
            'waktu_selesai' => 'required',
        ]);

        $pembimbing = new CareerGuidanceSchedule([
            'career_guidance_user_id' => $request->get('career_guidance_user_id'),
            'hari' => $request->get('hari'),
            'waktu_awal' => $request->get('waktu_awal'),
            'waktu_selesai' => $request->get('waktu_selesai'),
            'catatan' => $request->get('catatan'),
        ]);

        $pembimbing->save();
        return redirect('/career_guidance_schedules')->with('success', 'Jadwal Bimbingan berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['career_guidance_users'] = CareerGuidanceUser::all();
        $data['detail'] = CareerGuidanceSchedule::findOrFail($id);
        return view('back.career_guidance_schedules.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'career_guidance_user_id' => 'required',
            'hari' => 'required',
            'waktu_awal' => 'required',
            'waktu_selesai' => 'required',
        ]);
        
        $jadwal = CareerGuidanceSchedule::findOrFail($id);

        $jadwal->career_guidance_user_id =  $request->get('career_guidance_user_id');
        $jadwal->hari = $request->get('hari');
        $jadwal->waktu_awal = $request->get('waktu_awal');
        $jadwal->waktu_selesai = $request->get('waktu_selesai');
        $jadwal->catatan = $request->get('catatan');

        if ($jadwal->save()) {
            return redirect('/career_guidance_schedules')->with('success', 'Jadwal Bimbingan berhasil diperbaharui!');
        }else{
            return redirect('/career_guidance_schedules')->with('error', 'Jadwal Bimbingan gagal diperbaharui!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwal = CareerGuidanceSchedule::findOrFail($id);
        if ($jadwal->delete()) {
            return redirect('/career_guidance_schedules')->with('success', 'Jadwal Bimbingan berhasil dihapus!');
        }else{
            return redirect('/career_guidance_schedules')->with('error', 'Jadwal Bimbingan gagal dihapus!');
        }
    }
}
