<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerGuidanceSchedule extends Model
{
    protected $fillable = [
        'career_guidance_user_id', 'hari', 'waktu_awal', 'waktu_selesai', 'catatan'
    ];

    public function pembimbing(){
        return $this->belongsTo(CareerGuidanceUser::class, 'career_guidance_user_id');
    }
}
