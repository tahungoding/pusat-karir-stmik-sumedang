<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareerGuidanceScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_guidance_schedules', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('career_guidance_user_id')->unsigned();
            $table->enum('hari', ['senin','selasa','rabu','kamis','jumat','sabtu'])->unsigned();
            $table->time('waktu_awal');
            $table->time('waktu_selesai');
            $table->string('catatan')->nullable();
            $table->timestamps();
            $table->foreign('career_guidance_user_id')->references('id')->on('career_guidance_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_guidance_schedule');
    }
}
