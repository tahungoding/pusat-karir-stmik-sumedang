<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareerGuidanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_guidances', function (Blueprint $table) {
            $table->id();
            $table->string('nim')->unique();
            $table->enum('program_studi', ['Teknik Informatika', 'Sistem Informasi', 'Manajemen Informasi']);
            $table->bigInteger('career_guidance_user_id')->unsigned();
            $table->string('nama_lengkap');
            $table->enum('jenis_kelamin', ['Pria', 'Wanita']);
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('telepon');
            $table->string('email')->unique()->nullable();
            $table->text('area_permasalahan')->nullable();
            $table->enum('sesi', ['individual', 'kelompok', 'keduanya'])->nullable();
            $table->string('alasan_sesi')->nullable();
            $table->string('keterangan_diri')->nullable();
            $table->string('cv')->nullable();
            $table->timestamps();
            $table->foreign('career_guidance_user_id')->references('id')->on('career_guidance_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_guidance');
    }
}
